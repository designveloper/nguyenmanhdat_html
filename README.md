* [Entity](https://www.w3schools.com/html/html_entities.asp)
* Add more spaces with &nbsp
* Download link 

        <a href="_assets/syntax.pdf" title="HTML syntax reference" download="HTML_Syntax">Download</a>

* Definition list

        <dl>
          <dt>Unordered list </dt>
          <dd>Grouping of list items in no specific order</dd>
        </dl>
